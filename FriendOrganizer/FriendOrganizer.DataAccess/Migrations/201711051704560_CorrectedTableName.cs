namespace FriendOrganizer.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectedTableName : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ProgramingLanguage", newName: "ProgrammingLanguage");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ProgrammingLanguage", newName: "ProgramingLanguage");
        }
    }
}
