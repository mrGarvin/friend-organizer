﻿using System.Windows;
using FriendOrganizer.UI.ViewModels;
using MahApps.Metro.Controls;

namespace FriendOrganizer.UI.Views
{
    public partial class MainWindow : MetroWindow
    {
        private MainWindowViewModel _viewModel;

        public MainWindow(MainWindowViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel;
            DataContext = viewModel;
            Loaded += MainWindow_Loaded;
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e) => await _viewModel.Load();
    }
}
