﻿using System;
using Prism.Events;

namespace FriendOrganizer.UI.Event
{
    public class MeetingDateToChangedEvent : PubSubEvent<MeetingDateToChangedEventArgs>
    {
    }

    public class MeetingDateToChangedEventArgs
    {
        public int MeetingId { get; }
        public DateTime NewDateTo { get; }

        public MeetingDateToChangedEventArgs(int meetingId, DateTime newDateTo)
        {
            MeetingId = meetingId;
            NewDateTo = newDateTo;
        }
    }
}