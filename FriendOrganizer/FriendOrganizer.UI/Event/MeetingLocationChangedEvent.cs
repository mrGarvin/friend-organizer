﻿using FriendOrganizer.UI.Enums;
using Prism.Events;

namespace FriendOrganizer.UI.Event
{
    public class MeetingLocationChangedEvent : PubSubEvent<MeetingLocationChangedEventArgs>
    {
    }

    public class MeetingLocationChangedEventArgs
    {
        public int MeetingId { get; }
        public WeatherLocation NewLocation { get; }

        public MeetingLocationChangedEventArgs(int meetingId, WeatherLocation newLocation)
        {
            MeetingId = meetingId;
            NewLocation = newLocation;
        }
    }
}