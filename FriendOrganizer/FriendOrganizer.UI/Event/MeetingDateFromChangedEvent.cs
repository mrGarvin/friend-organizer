﻿using System;
using Prism.Events;

namespace FriendOrganizer.UI.Event
{
    public class MeetingDateFromChangedEvent : PubSubEvent<MeetingDateFromChangedEventArgs>
    {
    }

    public class MeetingDateFromChangedEventArgs
    {
        public int MeetingId { get; }
        public DateTime NewDateFrom { get; }

        public MeetingDateFromChangedEventArgs(int meetingId, DateTime newDateFrom)
        {
            MeetingId = meetingId;
            NewDateFrom = newDateFrom;
        }
    }
}