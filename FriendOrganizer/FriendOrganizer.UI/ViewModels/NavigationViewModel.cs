﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Event;
using FriendOrganizer.UI.Interfaces.Data.Lookups;
using FriendOrganizer.UI.Interfaces.ViewModels;
using Prism.Events;
using Prism.Mvvm;

namespace FriendOrganizer.UI.ViewModels
{
    public class NavigationViewModel : BindableBase, INavigationViewModel
    {
        private readonly IFriendLookupDataService _friendLookupService;
        private readonly IEventAggregator _eventAggregator;
        private readonly IMeetingLookupDataService _meetingLookupService;

        public ObservableCollection<NavigationItemViewModel> Friends { get; protected set; }

        public ObservableCollection<NavigationItemViewModel> Meetings { get; protected set; }

        public NavigationViewModel(IFriendLookupDataService friendLookupService,
            IMeetingLookupDataService meetingLookupService,
            IEventAggregator eventAggregator)
        {
            _friendLookupService = friendLookupService;
            _meetingLookupService = meetingLookupService;
            _eventAggregator = eventAggregator;
            Friends = new ObservableCollection<NavigationItemViewModel>();
            Meetings = new ObservableCollection<NavigationItemViewModel>();
            _eventAggregator.GetEvent<AfterDetailSavedEvent>().Subscribe(AfterDetailSaved);
            _eventAggregator.GetEvent<AfterDetailDeletedEvent>().Subscribe(AfterDetailDeleted);
        }

        public async Task LoadAsync()
        {
            var lookup = await _friendLookupService.GetFriendLookupAsync();
            PopulateWith(Friends, lookup, nameof(FriendDetailViewModel));

            lookup = await _meetingLookupService.GetMeetingLookupAsync();
            PopulateWith(Meetings, lookup, nameof(MeetingDetailViewModel));
        }

        private void AfterDetailSaved(AfterDetailSavedEventArgs args)
        {
            switch (args.ViewModelName)
            {
                case nameof(FriendDetailViewModel):
                    AfterDetailSaved(Friends, args);
                    break;
                case nameof(MeetingDetailViewModel):
                    AfterDetailSaved(Meetings, args);
                    break;
            }
        }

        private void AfterDetailDeleted(AfterDetailDeletedEventArgs args)
        {
            switch (args.ViewModelName)
            {
                case nameof(FriendDetailViewModel):
                    AfterDetailDeleted(Friends, args);
                    break;
                case nameof(MeetingDetailViewModel):
                    AfterDetailDeleted(Meetings, args);
                    break;
            }
        }

        private void PopulateWith(ObservableCollection<NavigationItemViewModel> observableCollection, IEnumerable<LookupItem> lookupItems, string detailViewModelName)
        {
            observableCollection.Clear();
            foreach (var item in lookupItems)
            {
                observableCollection.Add(new NavigationItemViewModel(item.Id, item.DisplayMember, detailViewModelName, _eventAggregator));
            }
        }

        private void AfterDetailSaved(ObservableCollection<NavigationItemViewModel> items, AfterDetailSavedEventArgs args)
        {
            var item = items.SingleOrDefault(l => l.Id == args.Id);
            if (item is null) items.Add(new NavigationItemViewModel(args.Id, args.DisplayMember, args.ViewModelName, _eventAggregator));
            else item.DisplayMember = args.DisplayMember;
        }

        private void AfterDetailDeleted(ObservableCollection<NavigationItemViewModel> items, AfterDetailDeletedEventArgs args)
        {
            var item = items.SingleOrDefault(i => i.Id == args.Id);
            if (item is null) return;
            items.Remove(item);
        }
    }
}