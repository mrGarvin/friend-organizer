﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using FriendOrganizer.UI.Enums;
using FriendOrganizer.UI.Event;
using FriendOrganizer.UI.Interfaces.ViewModels;
using FriendOrganizer.UI.Wrapper;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using WeatherAPI.Entities;
using WeatherAPI.Interfaces.Services;

namespace FriendOrganizer.UI.ViewModels
{
    public class WeatherViewModel : BindableBase, IWeatherViewModel
    {
        private static readonly HashSet<Weather> StoredWeather = new HashSet<Weather>(new WeatherEqualityComparer());

        private readonly IEventAggregator _eventAggregator;
        private readonly IWeatherService _weatherService;

        private DateTime _dateFrom;
        private DateTime _dateTo;
        private WeatherLocation _location;

        public int MeetingId { get; }

        public DelegateCommand RefreshWeatherCommand { get; }

        public ObservableCollection<WeatherWrapper> Weather { get; }

        public WeatherViewModel(int meetingId, DateTime dateFrom, DateTime dateTo, WeatherLocation location, IEventAggregator eventAggregator, IWeatherService weatherService)
        {
            MeetingId = meetingId;
            _dateFrom = dateFrom;
            _dateTo = dateTo;
            _location = location;
            _eventAggregator = eventAggregator;
            _weatherService = weatherService;

            RefreshWeatherCommand = new DelegateCommand(OnRefreshWeatherExecute, OnRefreshWeatherCanExecute);

            Weather = new ObservableCollection<WeatherWrapper>();
            Init();
        }

        private async void Init()
        {
            _eventAggregator.GetEvent<MeetingDateFromChangedEvent>().Subscribe(OnMeetingDateFromChanged);
            _eventAggregator.GetEvent<MeetingDateToChangedEvent>().Subscribe(OnMeetingDateToChanged);
            _eventAggregator.GetEvent<MeetingLocationChangedEvent>().Subscribe(OnMeetingLocationChanged);
            await UpdateWeather();
        }

        private async void OnMeetingDateFromChanged(MeetingDateFromChangedEventArgs args)
        {
            if (args.MeetingId != MeetingId) return;
            if (_dateFrom == args.NewDateFrom) return;
            _dateFrom = args.NewDateFrom;
            await UpdateWeather();
        }

        private async void OnMeetingDateToChanged(MeetingDateToChangedEventArgs args)
        {
            if (args.MeetingId != MeetingId) return;
            if (_dateTo == args.NewDateTo) return;
            _dateTo = args.NewDateTo;
            await UpdateWeather();
        }

        private async void OnMeetingLocationChanged(MeetingLocationChangedEventArgs args)
        {
            if (args.MeetingId != MeetingId) return;
            if (_location == args.NewLocation) return;
            _location = args.NewLocation;
            RefreshWeatherCommand.RaiseCanExecuteChanged();
            await UpdateWeather();
        }

        private async void OnRefreshWeatherExecute()
        {
            lock (StoredWeather)
            {
                StoredWeather.Clear();
            }
            await UpdateWeather();
        }

        private bool OnRefreshWeatherCanExecute() => _location != WeatherLocation.Unspecified;

        private async Task UpdateWeather()
        {
            Weather.Clear();
            if (_location == WeatherLocation.Unspecified) return;
            DateTime date = _dateFrom;
            while (date <= _dateTo)
            {
                Weather weather = await GetWeatherAt(date);
                Store(weather);
                Weather.Add(new WeatherWrapper(weather));
                if (weather.Predictability == -1) return;
                date = date.AddDays(1);
            }
        }

        private async Task<Weather> GetWeatherAt(DateTime date)
        {
            Weather weather;
            lock (StoredWeather)
            {
                weather = StoredWeather.FirstOrDefault(w => w.Date == date.Date && w.Location == _location.ToString());
            }
            if (weather is null) weather = await _weatherService.GetWeatherOn(date, _location.ToString());
            return weather;
        }

        private void Store(Weather weather)
        {
            lock (StoredWeather)
            {
                StoredWeather.Add(weather);
            }
        }

        private class WeatherEqualityComparer : IEqualityComparer<Weather>
        {
            public bool Equals(Weather weather1, Weather weather2)
            {
                if (weather1 is null && weather2 is null) return true;
                if (weather1 is null | weather2 is null) return false;
                return weather1.Date == weather2.Date && weather1.Location == weather2.Location;
            }

            public int GetHashCode(Weather weather) => weather.Date.GetHashCode() ^ weather.Location.GetHashCode();
        }
    }
}