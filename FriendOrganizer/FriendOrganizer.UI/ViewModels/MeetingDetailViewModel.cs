﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Enums;
using FriendOrganizer.UI.Event;
using FriendOrganizer.UI.Interfaces.Data.Repositories;
using FriendOrganizer.UI.Interfaces.Services;
using FriendOrganizer.UI.Interfaces.ViewModels;
using FriendOrganizer.UI.Utilities;
using FriendOrganizer.UI.Views.Services;
using FriendOrganizer.UI.Wrapper;
using Prism.Commands;
using Prism.Events;
using WeatherAPI.Interfaces.Services;

namespace FriendOrganizer.UI.ViewModels
{
    public class MeetingDetailViewModel : DetailViewModelBase, IMeetingDetailViewModel
    {
        private readonly IMeetingRepository _meetingRepository;
        private Friend _selectedAvailableFriend;
        private Friend _selectedAddedFriend;
        private WeatherLocationWrapper _selectedLocation;

        private MeetingWrapper _meeting;
        private List<Friend> _allFriends;
        private IWeatherService _weatherService;

        public MeetingWrapper Meeting
        {
            get => _meeting;
            set => SetProperty(ref _meeting, value);
        }

        public Friend SelectedAddedFriend
        {
            get => _selectedAddedFriend;
            set => SetProperty(ref _selectedAddedFriend, value, RemoveFriendCommand.RaiseCanExecuteChanged);
        }

        public Friend SelectedAvailableFriend
        {
            get => _selectedAvailableFriend;
            set => SetProperty(ref _selectedAvailableFriend, value, AddFriendCommand.RaiseCanExecuteChanged);
        }

        public WeatherLocationWrapper SelectedLocation
        {
            get => _selectedLocation;
            set => SetProperty(ref _selectedLocation, value, () => Meeting.Location = SelectedLocation.Value);
        }

        public ObservableCollection<Friend> AddedFriends { get; }

        public ObservableCollection<Friend> AvailableFriends { get; }

        public ObservableCollection<WeatherLocationWrapper> Locations { get; }

        public DelegateCommand AddFriendCommand { get; }

        public DelegateCommand RemoveFriendCommand { get; }

        public IWeatherViewModel WeatherViewModel { get; private set; }

        public MeetingDetailViewModel(IEventAggregator eventAggregator,
                                      IMessageDialogService messageDialogService,
                                      IMeetingRepository meetingRepository,
                                      IWeatherService weatherService) : base(eventAggregator, messageDialogService)
        {
            _meetingRepository = meetingRepository;
            _weatherService = weatherService;

            EventAggregator.GetEvent<AfterDetailSavedEvent>().Subscribe(AfterDetailSaved);
            EventAggregator.GetEvent<AfterDetailDeletedEvent>().Subscribe(AfterDetailDeleted);

            AddedFriends = new ObservableCollection<Friend>();
            AvailableFriends = new ObservableCollection<Friend>();
            Locations = new ObservableCollection<WeatherLocationWrapper>(GetWeatherLocationList());
            AddFriendCommand = new DelegateCommand(OnAddFriendExecute, OnAddFriendCanExecute);
            RemoveFriendCommand = new DelegateCommand(OnRemoveFriendExecute, OnRemoveFriendCanExecute);

            PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == nameof(SelectedLocation))
                    EventAggregator.GetEvent<MeetingLocationChangedEvent>()
                        .Publish(new MeetingLocationChangedEventArgs(Id, SelectedLocation.Value));
            };
        }

        public override async Task LoadAsync(int meetingId)
        {
            DateTime now = DateTime.Now.TruncateMilliseconds();
            var meeting = meetingId > 0
                ? await _meetingRepository.GetByIdAsync(meetingId)
                : CreateNewMeeting(now);

            Id = meetingId;

            InitializeMeeting(meeting, now);

            _allFriends = await _meetingRepository.GetAllFriendsAsync();
            SetupPicklist();

            SelectedLocation = Locations.First(wlw => wlw.Value == Meeting.Location);

            WeatherViewModel = new WeatherViewModel(Id, Meeting.DateFrom, Meeting.DateTo, Meeting.Location, EventAggregator, _weatherService);
        }

        protected override async void OnSaveExecute()
        {
            await _meetingRepository.SaveAsync();
            Id = Meeting.Id;
            HasChanges = _meetingRepository.HasChanges();
            Meeting.CanEditMeeting = Meeting.CanEditDateFrom || !Meeting.CanEditDateTo && !HasChanges;
            RaiseDetailSavedEvent(Meeting.Id, Meeting.Title);
        }

        protected override bool OnSaveCanExecute()
        {
            return Meeting != null && !Meeting.HasErrors && HasChanges;
        }

        protected override async void OnDeleteExecute()
        {
            var result = await MessageDialogService.ShowOkCancelDialogAsync($"Do you really want to delete the meeting {Meeting.Title}?", "Question");
            if (result != MessageDialogResult.OK) return;
            _meetingRepository.Remove(Meeting.Model);
            await _meetingRepository.SaveAsync();
            RaiseDetailDeletedEvent(Meeting.Id);
        }

        private Meeting CreateNewMeeting(DateTime now)
        {
            var meeting = new Meeting
            {
                DateFrom = now,
                DateTo = now.AddHours(1)
            };
            _meetingRepository.Add(meeting);
            return meeting;
        }

        private void InitializeMeeting(Meeting meeting, DateTime now)
        {
            Meeting = new MeetingWrapper(meeting, now);
            Meeting.PropertyChanged += (s, e) =>
            {
                if (!HasChanges) HasChanges = _meetingRepository.HasChanges();
                switch (e.PropertyName)
                {
                    case nameof(Meeting.HasErrors):
                        SaveCommand.RaiseCanExecuteChanged();
                        break;
                    case nameof(Meeting.Title):
                        Title = Meeting.Title;
                        break;
                    case nameof(Meeting.DateFrom):
                        EventAggregator.GetEvent<MeetingDateFromChangedEvent>().Publish(new MeetingDateFromChangedEventArgs(Id, Meeting.DateFrom));
                        break;
                    case nameof(Meeting.DateTo):
                        EventAggregator.GetEvent<MeetingDateToChangedEvent>().Publish(new MeetingDateToChangedEventArgs(Id, Meeting.DateTo));
                        break;
                }
            };
            SaveCommand.RaiseCanExecuteChanged();
            Meeting.CanEditMeeting = Meeting.CanEditDateFrom || !Meeting.CanEditDateTo && !HasChanges;

            if (Meeting.Id == 0)
            {
                // Little trick to trigger the validation!
                Meeting.Title = "";
            }
            Title = Meeting.Title;

            EventAggregator.GetEvent<MeetingDateFromChangedEvent>().Publish(new MeetingDateFromChangedEventArgs(Id, Meeting.DateFrom));
            EventAggregator.GetEvent<MeetingDateToChangedEvent>().Publish(new MeetingDateToChangedEventArgs(Id, Meeting.DateTo));
        }

        private void OnAddFriendExecute()
        {
            var friendToAdd = SelectedAvailableFriend;

            Meeting.Model.Friends.Add(friendToAdd);
            AddedFriends.Add(friendToAdd);
            AvailableFriends.Remove(friendToAdd);
            HasChanges = _meetingRepository.HasChanges();
            SaveCommand.RaiseCanExecuteChanged();
        }

        private bool OnAddFriendCanExecute() => SelectedAvailableFriend != null;

        private void OnRemoveFriendExecute()
        {
            var friendToRemove = SelectedAddedFriend;

            Meeting.Model.Friends.Remove(friendToRemove);
            AddedFriends.Remove(friendToRemove);
            AvailableFriends.Add(friendToRemove);
            HasChanges = _meetingRepository.HasChanges();
            SaveCommand.RaiseCanExecuteChanged();
        }

        private bool OnRemoveFriendCanExecute() => SelectedAddedFriend != null;

        private void SetupPicklist()
        {
            var meetingFriendIds = Meeting.Model.Friends.Select(f => f.Id).ToList();
            var addedFriends = _allFriends.Where(f => meetingFriendIds.Contains(f.Id)).OrderBy(f => f.FirstName);
            var availableFriends = _allFriends.Except(addedFriends).OrderBy(f => f.FirstName);

            PopulateWith(AddedFriends, addedFriends);
            PopulateWith(AvailableFriends, availableFriends);
        }

        private void PopulateWith(ObservableCollection<Friend> collection, IEnumerable<Friend> friends)
        {
            collection.Clear();
            foreach (var friend in friends)
            {
                collection.Add(friend);
            }
        }

        private async void AfterDetailSaved(AfterDetailSavedEventArgs args)
        {
            if (args.ViewModelName != nameof(FriendDetailViewModel)) return;
            await _meetingRepository.ReloadFriendAsync(args.Id);
            _allFriends = await _meetingRepository.GetAllFriendsAsync();
            SetupPicklist();
        }

        private async void AfterDetailDeleted(AfterDetailDeletedEventArgs args)
        {
            if (args.ViewModelName != nameof(FriendDetailViewModel)) return;
            _allFriends = await _meetingRepository.GetAllFriendsAsync();
            SetupPicklist();
        }

        private IEnumerable<WeatherLocationWrapper> GetWeatherLocationList()
        {
            foreach (WeatherLocation location in Enum.GetValues(typeof(WeatherLocation)).Cast<WeatherLocation>())
            {
                yield return new WeatherLocationWrapper(location);
            }
        }
    }
}