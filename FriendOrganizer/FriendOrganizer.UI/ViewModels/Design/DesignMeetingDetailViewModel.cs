﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Enums;
using FriendOrganizer.UI.Interfaces.Data.Repositories;
using FriendOrganizer.UI.Interfaces.Services;
using FriendOrganizer.UI.Interfaces.ViewModels;
using FriendOrganizer.UI.Utilities;
using FriendOrganizer.UI.Wrapper;
using Prism.Events;
using WeatherAPI.Interfaces.Services;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public class DesignMeetingDetailViewModel : MeetingDetailViewModel
    {
        public DesignMeetingDetailViewModel()
            : base(
                DVMDependencyFactory.Resolve<IEventAggregator>(),
                DVMDependencyFactory.Resolve<IMessageDialogService>(),
                DVMDependencyFactory.Resolve<IMeetingRepository>(),
                DVMDependencyFactory.Resolve<IWeatherService>()
            )
        {
            Id = 2;
            Title = "Meeting";
        }

        public new int Id { get; }
        public new string Title { get; }

        public new MeetingWrapper Meeting =>
            new MeetingWrapper(new Meeting {Id = 2, DateFrom = DateTime.Now.AddMinutes(1), DateTo = DateTime.Now.AddMinutes(3), Title = "Meeting"}, DateTime.Now.TruncateMilliseconds());

        public new Friend SelectedAddedFriend =>
            new Friend {Id = 1, FirstName = "Test", LastName = "Testing"};

        public new Friend SelectedAvailableFriend =>
            new Friend {Id = 2, FirstName = "James", LastName = "Bond"};

        public new ObservableCollection<Friend> AddedFriends =>
            new ObservableCollection<Friend>
            {
                SelectedAddedFriend
            };

        public new ObservableCollection<Friend> AvailableFriends =>
            new ObservableCollection<Friend>
            {
                SelectedAvailableFriend
            };

        public new IWeatherViewModel WeatherViewModel => DVMDependencyFactory.Resolve<IWeatherViewModel>();

        public new WeatherLocationWrapper SelectedLocation
        {
            get => Locations.First(wlw => wlw.Value == WeatherLocation.Gothenburg);
            set { }
        }
    }
}