﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac.Features.Indexed;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Enums;
using FriendOrganizer.UI.Interfaces.Data.Lookups;
using FriendOrganizer.UI.Interfaces.Data.Repositories;
using FriendOrganizer.UI.Interfaces.Services;
using FriendOrganizer.UI.Interfaces.ViewModels;
using FriendOrganizer.UI.Views.Services;
using Prism.Events;
using WeatherAPI.Entities;
using WeatherAPI.Interfaces.Services;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public static class DVMDependencyFactory
    {
        private static readonly IReadOnlyDictionary<Type, object> TypeObjectMapper;

        static DVMDependencyFactory()
        {
            TypeObjectMapper = new Dictionary<Type, object>
            {
                { typeof(IFriendRepository), new FakeRepository() },
                { typeof(IMeetingRepository), new FakeRepository() },
                { typeof(IProgrammingLanguageRepository), new FakeRepository() },
                { typeof(IEventAggregator), new FakeEventAggregator() },
                { typeof(IMessageDialogService), new FakeMessageDialogService() },
                { typeof(IFriendLookupDataService), new FakeLookupDataService() },
                { typeof(IMeetingLookupDataService), new FakeLookupDataService() },
                { typeof(IProgrammingLanguageLookupDataService), new FakeLookupDataService() },
                { typeof(INavigationViewModel), new DesignNavigationViewModel(new FakeLookupDataService(), new FakeLookupDataService(), new FakeEventAggregator()) },
                { typeof(IIndex<string, IDetailViewModel>), new FakeIndex() },
                { typeof(IWeatherService), new FakeWeatherService() },
                { typeof(IWeatherViewModel), new DesignWeatherViewModel(new FakeEventAggregator(), new FakeWeatherService()) }
            };
        }

        public static TObject Resolve<TObject>()
        {
            var obj = TypeObjectMapper[typeof(TObject)];
            var result = (TObject) obj;
            return result;
        }

        private class FakeRepository : IFriendRepository, IMeetingRepository, IProgrammingLanguageRepository
        {
            Task<IEnumerable<Friend>> IGenericRepository<Friend>.GetAllAsync()
            {
                return null;
            }

            Task<ProgrammingLanguage> IGenericRepository<ProgrammingLanguage>.GetByIdAsync(int id)
            {
                return null;
            }

            Task IGenericRepository<ProgrammingLanguage>.SaveAsync()
            {
                return null;
            }

            bool IGenericRepository<ProgrammingLanguage>.HasChanges()
            {
                return false;
            }

            public void Add(ProgrammingLanguage model)
            {
            }

            public void Remove(ProgrammingLanguage model)
            {
            }

            public Task<bool> IsReferencedByFriendAsync(int programmingLanguageId)
            {
                return null;
            }

            Task<IEnumerable<ProgrammingLanguage>> IGenericRepository<ProgrammingLanguage>.GetAllAsync()
            {
                return null;
            }

            Task<Meeting> IGenericRepository<Meeting>.GetByIdAsync(int id)
            {
                return null;
            }

            Task IGenericRepository<Meeting>.SaveAsync()
            {
                return null;
            }

            bool IGenericRepository<Meeting>.HasChanges()
            {
                return false;
            }

            public void Add(Meeting model)
            {
            }

            public void Remove(Meeting model)
            {
            }

            Task<IEnumerable<Meeting>> IGenericRepository<Meeting>.GetAllAsync()
            {
                return null;
            }

            Task<Friend> IGenericRepository<Friend>.GetByIdAsync(int id)
            {
                return null;
            }

            Task IGenericRepository<Friend>.SaveAsync()
            {
                return null;
            }

            bool IGenericRepository<Friend>.HasChanges()
            {
                return false;
            }

            public void Add(Friend model)
            {
            }

            public void Remove(Friend model)
            {
            }

            public void RemovePhoneNumber(FriendPhoneNumber model)
            {
            }

            public Task<bool> HasMeetingsAsync(int friendId)
            {
                return null;
            }

            public Task<List<Friend>> GetAllFriendsAsync()
            {
                return null;
            }

            public Task ReloadFriendAsync(int friendId)
            {
                return null;
            }
        }

        private class FakeMessageDialogService : IMessageDialogService
        {
            public Task<MessageDialogResult> ShowOkCancelDialogAsync(string text, string title)
            {
                return null;
            }

            public Task ShowInfoDialogAsync(string text)
            {
                return null;
            }
        }

        private class FakeLookupDataService : IFriendLookupDataService, IMeetingLookupDataService, IProgrammingLanguageLookupDataService
        {
            public Task<IEnumerable<LookupItem>> GetFriendLookupAsync()
            {
                return null;
            }

            public Task<List<LookupItem>> GetMeetingLookupAsync()
            {
                return null;
            }

            public Task<IEnumerable<LookupItem>> GetProgrammingLanguageLookupAsync()
            {
                return null;
            }
        }

        private class FakeIndex : IIndex<string, IDetailViewModel>
        {
            public bool TryGetValue(string key, out IDetailViewModel value)
            {
                throw new NotImplementedException();
            }

            public IDetailViewModel this[string key]
            {
                get { throw new NotImplementedException(); }
            }
        }

        private class FakeEventAggregator : IEventAggregator
        {
            public TEventType GetEvent<TEventType>() where TEventType : EventBase, new() => new TEventType();
        }

        private class FakeWeatherService : IWeatherService
        {
            public async Task<Weather> GetWeatherOn(DateTime date, string location)
            {
                return await Task.Run(() => new NullWeather(DateTime.Now, WeatherLocation.Gothenburg.ToString()));
            }
        }
    }
}