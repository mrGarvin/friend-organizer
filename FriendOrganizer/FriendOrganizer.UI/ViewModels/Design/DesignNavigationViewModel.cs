﻿using System.Collections.ObjectModel;
using FriendOrganizer.UI.Interfaces.Data.Lookups;
using Prism.Events;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public class DesignNavigationViewModel : NavigationViewModel
    {
        public DesignNavigationViewModel()
            : base(
                  DVMDependencyFactory.Resolve<IFriendLookupDataService>(),
                  DVMDependencyFactory.Resolve<IMeetingLookupDataService>(),
                  DVMDependencyFactory.Resolve<IEventAggregator>()
                  )
        {
            SetProperties();
        }

        public DesignNavigationViewModel(IFriendLookupDataService friendLookupService,
                                         IMeetingLookupDataService meetingLookupService,
                                         IEventAggregator eventAggregator)
            : base(friendLookupService, meetingLookupService, eventAggregator)
        {
            SetProperties();
        }

        private void SetProperties()
        {
            Friends = new ObservableCollection<NavigationItemViewModel>
            {
                new NavigationItemViewModel(1, "Test Testing", null, null),
                new NavigationItemViewModel(2, "James Bond", null, null)
            };
            Meetings = new ObservableCollection<NavigationItemViewModel>
            {
                new NavigationItemViewModel(1, "Test Meeting", null, null),
                new NavigationItemViewModel(2, "Save the World", null, null)
            };
        }
    }
}