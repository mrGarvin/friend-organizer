﻿using System.Collections.ObjectModel;
using Autofac.Features.Indexed;
using FriendOrganizer.UI.Interfaces.Services;
using FriendOrganizer.UI.Interfaces.ViewModels;
using Prism.Events;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public class DesignMainWindowViewModel : MainWindowViewModel
    {
        public DesignMainWindowViewModel()
            : base(
                  DVMDependencyFactory.Resolve<INavigationViewModel>(),
                  DVMDependencyFactory.Resolve<IIndex<string, IDetailViewModel>>(),
                  DVMDependencyFactory.Resolve<IEventAggregator>(),
                  DVMDependencyFactory.Resolve<IMessageDialogService>()
                  )
        {
        }

        public new ObservableCollection<IDetailViewModel> DetailViewModels =>
            new ObservableCollection<IDetailViewModel>
            {
                SelectedDetailViewModel,
                new DesignMeetingDetailViewModel()
            };
        public new INavigationViewModel NavigationViewModel =>
            new DesignNavigationViewModel();

        public new IDetailViewModel SelectedDetailViewModel
        {
            get => new DesignFriendDetailViewModel();
            set { }
        }
    }
}