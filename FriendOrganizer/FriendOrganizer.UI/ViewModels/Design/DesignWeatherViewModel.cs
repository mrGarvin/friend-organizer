﻿using System;
using System.Collections.ObjectModel;
using FriendOrganizer.UI.Enums;
using FriendOrganizer.UI.Wrapper;
using Prism.Events;
using WeatherAPI.Entities;
using WeatherAPI.Interfaces.Services;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public class DesignWeatherViewModel : WeatherViewModel
    {
        public DesignWeatherViewModel()
            : base(
                  1,
                  DateTime.Now,
                  DateTime.Now.AddDays(1),
                  WeatherLocation.Gothenburg,
                  DVMDependencyFactory.Resolve<IEventAggregator>(),
                  DVMDependencyFactory.Resolve<IWeatherService>()
                  )
        {
        }

        public DesignWeatherViewModel(IEventAggregator eventAggregator, IWeatherService weatherService)
            : base(1, DateTime.Now, DateTime.Now.AddDays(1), WeatherLocation.Gothenburg, eventAggregator, weatherService)
        {
        }

        public new ObservableCollection<WeatherWrapper> Weather => new ObservableCollection<WeatherWrapper>
            {
                new WeatherWrapper(new Weather(DateTime.Now, 2, 3, 4, "Showers", 50, "https://www.metaweather.com/static/img/weather/png/64/s.png", WeatherLocation.Gothenburg.ToString())),
                new WeatherWrapper(new NullWeather(DateTime.Now.AddDays(1), WeatherLocation.Gothenburg.ToString())),
                new WeatherWrapper(new NullWeather(DateTime.Now.AddDays(2), WeatherLocation.Gothenburg.ToString())),
                new WeatherWrapper(new NullWeather(DateTime.Now.AddDays(3), WeatherLocation.Gothenburg.ToString())),
                new WeatherWrapper(new NullWeather(DateTime.Now.AddDays(4), WeatherLocation.Gothenburg.ToString()))
            };
    }
}