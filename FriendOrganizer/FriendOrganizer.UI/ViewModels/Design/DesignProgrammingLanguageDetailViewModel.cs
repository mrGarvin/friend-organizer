﻿using System.Collections.ObjectModel;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Interfaces.Data.Repositories;
using FriendOrganizer.UI.Interfaces.Services;
using FriendOrganizer.UI.Wrapper;
using Prism.Events;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public class DesignProgrammingLanguageDetailViewModel : ProgrammingLanguageDetailViewModel
    {
        public DesignProgrammingLanguageDetailViewModel()
            : base(
                  DVMDependencyFactory.Resolve<IEventAggregator>(),
                  DVMDependencyFactory.Resolve<IMessageDialogService>(),
                  DVMDependencyFactory.Resolve<IProgrammingLanguageRepository>()
                  )
        {
            
        }

        //public int Id { get; }
        //public string Title { get; }

        public new ObservableCollection<ProgrammingLanguageWrapper> ProgrammingLanguages =>
            new ObservableCollection<ProgrammingLanguageWrapper>
            {
                SelectedProgrammingLanguage,
                new ProgrammingLanguageWrapper(new ProgrammingLanguage{Id = 2, Name = "Java"})
            };

        public new ProgrammingLanguageWrapper SelectedProgrammingLanguage
        {
            get => new ProgrammingLanguageWrapper(new ProgrammingLanguage { Id = 1, Name = "C#" });
            set { }
        }
    }
}