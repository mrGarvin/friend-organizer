﻿using System.Collections.ObjectModel;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Interfaces.Data.Lookups;
using FriendOrganizer.UI.Interfaces.Data.Repositories;
using FriendOrganizer.UI.Interfaces.Services;
using FriendOrganizer.UI.Wrapper;
using Prism.Events;

namespace FriendOrganizer.UI.ViewModels.Design
{
    public class DesignFriendDetailViewModel : FriendDetailViewModel
    {
        public DesignFriendDetailViewModel()
            : base(DVMDependencyFactory.Resolve<IFriendRepository>(),
                   DVMDependencyFactory.Resolve<IEventAggregator>(),
                   DVMDependencyFactory.Resolve<IMessageDialogService>(),
                   DVMDependencyFactory.Resolve<IProgrammingLanguageLookupDataService>())
        {
            Id = 1;
            Title = "Friend";
        }

        public new int Id { get; }
        public new string Title { get; }

        public new FriendWrapper Friend
        {
            get => new FriendWrapper(new Friend { Id = 1, FirstName = "Test", LastName = "Testing", Email = "test@test.com", FavoriteLanguageId = 1 });
            set { }
        }

        public new FriendPhoneNumberWrapper SelectedPhoneNumber
        {
            get => new FriendPhoneNumberWrapper(new FriendPhoneNumber { Id = 1, FriendId = 1, Number = "+1234567890" });
            set { }
        }

        public new ObservableCollection<LookupItem> ProgrammingLanguages =>
            new ObservableCollection<LookupItem>
            {
                new LookupItem {Id = 1, DisplayMember = " - "},
                new LookupItem {Id = 2, DisplayMember = "C#"},
                new LookupItem {Id = 3, DisplayMember = "Java"}
            };

        public new ObservableCollection<FriendPhoneNumberWrapper> PhoneNumbers =>
            new ObservableCollection<FriendPhoneNumberWrapper>
            {
                SelectedPhoneNumber,
                new FriendPhoneNumberWrapper(new FriendPhoneNumber { Id = 2, FriendId = 1, Number = "+0987654321" })
            };
    }
}