﻿using System;
using System.Windows.Media;
using FriendOrganizer.UI.Enums;
using WeatherAPI.Entities;

namespace FriendOrganizer.UI.Wrapper
{
    public class WeatherWrapper
    {
        private bool IsNullWeather { get; }

        public string Date { get; }
        public string MinTemp { get; }
        public string MaxTemp { get; }
        public string Temp { get; }
        public string State { get; }
        public string Predictability { get; }
        public string ImageUrl { get; }
        public WeatherLocation Location { get; }
        
        public WeatherWrapper(Weather weather)
        {
            IsNullWeather = weather.Predictability == -1;
            Date = ValueToString(weather.Date);
            MinTemp = ValueToString(weather.MinTemp);
            MaxTemp = ValueToString(weather.MaxTemp);
            Temp = ValueToString(weather.Temp);
            State = ValueToString(weather.State);
            Predictability = ValueToString(weather.Predictability);
            ImageUrl = ValueToString(weather.ImageUrl); // TODO: Set default image?
            Location = ToWeatherLocation(weather.Location);
        }

        private string ValueToString<TValue>(TValue value)
        {
            switch (value)
            {
                case DateTime date:
                    return date.ToString("yyyy-MM-dd");
                case double temp when !IsNullWeather:
                    return $"{Math.Round(temp, 1)}°";
                case string str when !IsNullWeather:
                    return str;
                case int predictability when !IsNullWeather:
                    return $"({predictability}%)";
                default:
                    return "n/A";
            }
        }

        private WeatherLocation ToWeatherLocation(string str)
        {
            if (Enum.TryParse(str, true, out WeatherLocation location)) return location;
            return WeatherLocation.Unspecified;
        }
    }
}