﻿using System;
using FriendOrganizer.Model;
using FriendOrganizer.UI.Enums;

namespace FriendOrganizer.UI.Wrapper
{
    public class MeetingWrapper : ModelWrapperBase<Meeting>
    {
        private readonly DateTime _now;

        public int Id => Model.Id;

        public string Title
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public DateTime DateFrom
        {
            get => GetValue<DateTime>();
            set => SetValue(value);
        }

        public DateTime DateTo
        {
            get => GetValue<DateTime>();
            set => SetValue(value);
        }
        
        public DateTime MinimumDate => _now;

        public bool CanEditDateFrom => DateFrom >= _now;

        public bool CanEditDateTo => DateTo >= _now;

        public bool CanEditMeeting { get; set; }

        public WeatherLocation Location
        {
            get => GetLocation();
            set => SetLocation(value);
        }

        public MeetingWrapper(Meeting model, DateTime now) : base(model)
        {
            _now = now;
        }

        private WeatherLocation GetLocation() => Enum.TryParse(Model.Location, true, out WeatherLocation location) ? location : WeatherLocation.Unspecified;

        private void SetLocation(WeatherLocation value) => Model.Location = value == WeatherLocation.Unspecified ? null : value.ToString();
    }
}