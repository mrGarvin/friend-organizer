﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace FriendOrganizer.UI.Wrapper
{
    public abstract class ModelWrapperBase<T> : NotifyDataErrorInfoBase
    {
        public T Model { get; }

        protected ModelWrapperBase(T model)
        {
            Model = model;
        }

        protected virtual TValue GetValue<TValue>([CallerMemberName] string propertyName = null) =>
            (TValue) typeof(T).GetProperty(propertyName).GetValue(Model);

        protected virtual void SetValue<TValue>(TValue value, [CallerMemberName] string propertyName = null)
        {
            if (!OldValueEqualsNewValue(propertyName, value)) return;
            typeof(T).GetProperty(propertyName).SetValue(Model, value);
            RaisePropertyChanged(propertyName);
            ValidatePropertyInternal(propertyName, value);
        }

        protected virtual void SetValue<TValue>(TValue value, Action onSet, bool invokeActionAfterSet = true, [CallerMemberName] string propertyName = null)
        {
            if (!invokeActionAfterSet) onSet();
            SetValue(value, propertyName);
            if (invokeActionAfterSet) onSet();
        }

        private bool OldValueEqualsNewValue<TValue>(string propertyName, TValue newValue)
        {
            TValue oldValue = GetValue<TValue>(propertyName);
            if (ReferenceEquals(oldValue, newValue)) return false;
            return !(newValue is IEquatable<TValue> item) || !item.Equals(oldValue);
        }

        private void ValidatePropertyInternal(string propertyName, object currentValue)
        {
            ClearErrors(propertyName);

            ValidateDataAnnotations(propertyName, currentValue);

            ValidateCustomErrors(propertyName);
        }

        private void ValidateDataAnnotations(string propertyName, object currentValue)
        {
            var context = new ValidationContext(Model) { MemberName = propertyName };
            var results = new List<ValidationResult>();
            Validator.TryValidateProperty(currentValue, context, results);

            foreach (var result in results)
            {
                AddError(propertyName, result.ErrorMessage);
            }
        }

        private void ValidateCustomErrors(string propertyName)
        {
            var errors = ValidateProperty(propertyName);
            if (errors is null) return;
            foreach (var error in errors)
            {
                AddError(propertyName, error);
            }
        }

        protected virtual IEnumerable<string> ValidateProperty(string propertyName)
        {
            return null;
        }
    }
}