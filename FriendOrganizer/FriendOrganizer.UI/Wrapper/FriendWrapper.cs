﻿using System;
using System.Collections.Generic;
using FriendOrganizer.Model;

namespace FriendOrganizer.UI.Wrapper
{
    public class FriendWrapper : ModelWrapperBase<Friend>
    {
        public int Id => Model.Id;

        public string FirstName
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public string LastName
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public string Email
        {
            get => GetValue<string>();
            set => SetValue(value);
        }

        public int? FavoriteLanguageId
        {
            get => GetValue<int?>();
            set => SetValue(value);
        }

        public string FullName => Model is null
                                  ? null
                                  : $"{Model.FirstName}{(string.IsNullOrEmpty(Model.LastName) ? "" : $" {Model.LastName}")}";

        public FriendWrapper(Friend model) : base(model)
        {
        }

        protected override IEnumerable<string> ValidateProperty(string propertyName)
        {
            switch (propertyName)
            {
                case nameof(FirstName):
                    if (string.Equals(FirstName, "Robot", StringComparison.OrdinalIgnoreCase))
                        yield return "Robots are not valid friends";
                    break;
            }
        }
    }
}