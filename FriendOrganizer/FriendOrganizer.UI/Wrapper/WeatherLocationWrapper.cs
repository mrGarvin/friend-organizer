﻿using FriendOrganizer.UI.Enums;

namespace FriendOrganizer.UI.Wrapper
{
    public class WeatherLocationWrapper
    {
        public string DisplayMember { get; }

        public WeatherLocation Value { get; }

        public WeatherLocationWrapper(WeatherLocation value)
        {
            DisplayMember = value == WeatherLocation.Unspecified ? " - " : value.ToString();
            Value = value;
        }
    }
}