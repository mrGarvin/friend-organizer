﻿using System.Threading.Tasks;
using FriendOrganizer.UI.Views.Services;

namespace FriendOrganizer.UI.Interfaces.Services
{
    public interface IMessageDialogService
    {
        Task<MessageDialogResult> ShowOkCancelDialogAsync(string text, string title);
        Task ShowInfoDialogAsync(string text);
    }
}