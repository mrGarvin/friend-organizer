﻿using System.Threading.Tasks;
using Prism.Commands;

namespace FriendOrganizer.UI.Interfaces.ViewModels
{
    public interface IDetailViewModel
    {
        int Id { get; }
        string Title { get; }
        bool HasChanges { get; }
        DelegateCommand CloseDetailViewCommand { get; }

        Task LoadAsync(int id);
    }
}