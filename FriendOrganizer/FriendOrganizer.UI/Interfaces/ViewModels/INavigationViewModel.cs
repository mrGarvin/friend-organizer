﻿using System.Threading.Tasks;

namespace FriendOrganizer.UI.Interfaces.ViewModels
{
    public interface INavigationViewModel
    {
        Task LoadAsync();
    }
}