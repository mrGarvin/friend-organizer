﻿using WeatherAPI.Entities;

namespace FriendOrganizer.UI.Interfaces.ViewModels
{
    public interface IWeatherViewModel
    {
        int MeetingId { get; }
    }
}