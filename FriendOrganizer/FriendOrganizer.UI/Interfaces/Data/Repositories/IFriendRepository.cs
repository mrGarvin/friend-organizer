﻿using System.Threading.Tasks;
using FriendOrganizer.Model;

namespace FriendOrganizer.UI.Interfaces.Data.Repositories
{
    public interface IFriendRepository : IGenericRepository<Friend>
    {
        void RemovePhoneNumber(FriendPhoneNumber model);
        Task<bool> HasMeetingsAsync(int friendId);
    }
}