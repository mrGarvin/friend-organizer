﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace FriendOrganizer.UI.Interfaces.Data.Repositories
{
    public interface IGenericRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task SaveAsync();
        bool HasChanges();
        void Add(T model);
        void Remove(T model);
    }
}