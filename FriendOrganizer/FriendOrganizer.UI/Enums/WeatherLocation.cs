﻿namespace FriendOrganizer.UI.Enums
{
    public enum WeatherLocation
    {
        Unspecified,
        Gothenburg,
        Stockholm
    }
}