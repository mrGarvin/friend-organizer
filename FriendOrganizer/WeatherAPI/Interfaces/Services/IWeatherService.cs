﻿using System;
using System.Threading.Tasks;
using WeatherAPI.Entities;

namespace WeatherAPI.Interfaces.Services
{
    public interface IWeatherService
    {
        Task<Weather> GetWeatherOn(DateTime date, string location);
    }
}