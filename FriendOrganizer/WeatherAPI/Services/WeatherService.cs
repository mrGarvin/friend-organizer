﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using WeatherAPI.Entities;
using WeatherAPI.Interfaces.Services;

namespace WeatherAPI.Services
{
    public class WeatherService : IWeatherService
    {
        private const string BaseUrl = "https://www.metaweather.com/";
        private const string LocationQueryFormat = "location/search/?query={0}";
        private const string ImageUrlFormat = BaseUrl + "static/img/weather/png/64/{0}.png";
        private const string LocationUrlFormat = "location/{0}/";
        private const string SpecificDayUrlFormat = LocationUrlFormat + "{1}/{2}/{3}/";

        private readonly HttpClient _httpClient;
        private readonly Dictionary<string, int> _locationIdMapper;

        public WeatherService()
        {
            _httpClient = new HttpClient { BaseAddress = new Uri($"{BaseUrl}api/") };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            _locationIdMapper = new Dictionary<string, int>();

        }

        public async Task<Weather> GetWeatherOn(DateTime date, string location)
        {
            try
            {
                string path = await CreatePathFor(location, date);
                HttpResponseMessage response = await _httpClient.GetAsync(path);
                if (!response.IsSuccessStatusCode)
                    return CreateNullWeather(location, date);
                var result = await response.Content.ReadAsAsync<IEnumerable<JsonEntities.ConsolidatedWeather>>();
                return ToWeather(result.First(), location);
            }
            catch
            {
                return CreateNullWeather(location, date);
            }
        }

        private async Task<string> CreatePathFor(string location, DateTime date)
        {
            int locationId = await GetLocationIdFor(location);
            return string.Format(SpecificDayUrlFormat, locationId, date.Year, date.Month, date.Day);
        }

        private async Task<int> GetLocationIdFor(string location)
        {
            if (_locationIdMapper.TryGetValue(location, out int locationId))
                return locationId;
            string path = string.Format(LocationQueryFormat, location.ToLower());
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (!response.IsSuccessStatusCode)
                throw new HttpException($"Could not get location id for {location}.");
            var result = await response.Content.ReadAsAsync<IEnumerable<JsonEntities.LocationInformation>>();
            JsonEntities.LocationInformation locationInfo = result.FirstOrDefault();
            if (locationInfo is null || locationInfo.woeid == 0)
                throw new Exception($"Could not parse the content of the response:{Environment.NewLine}{response.Content}");
            locationId = locationInfo.woeid;
            _locationIdMapper.Add(location, locationId);
            return locationId;
        }
        
        private NullWeather CreateNullWeather(string location, DateTime date) => new NullWeather(date, location);

        private Weather ToWeather(JsonEntities.ConsolidatedWeather rawWeather, string location)
        {
            Weather weather = new Weather(DateTime.Parse(rawWeather.applicable_date), rawWeather.min_temp, rawWeather.max_temp, rawWeather.the_temp, rawWeather.weather_state_name, rawWeather.predictability, string.Format(ImageUrlFormat, rawWeather.weather_state_abbr), location);
            return weather;
        }
    }
}