﻿using System;

namespace WeatherAPI.Entities
{
    public class Weather
    {
        public DateTime Date { get; }
        public double MinTemp { get; }
        public double MaxTemp { get; }
        public double Temp { get; }
        public string State { get; }
        public int Predictability { get; }
        public string ImageUrl { get; }
        public string Location { get; }

        public Weather(DateTime date, double minTemp, double maxTemp, double temp, string state, int predictability, string imageUrl, string location)
        {
            Date = date;
            MinTemp = minTemp;
            MaxTemp = maxTemp;
            Temp = temp;
            State = state;
            Predictability = predictability;
            ImageUrl = imageUrl;
            Location = location;
        }
    }

    public class NullWeather : Weather
    {
        public NullWeather(DateTime date, string location)
            : base(date, default, default, default, default, -1, default, location)
        {
        }
    }
}
